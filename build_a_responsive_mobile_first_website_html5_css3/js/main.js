//animate smooth scroll
// ao clicar no botao view-work, vai para top das imagens
// posicao de images é guardada na variavel para ser utilizada
$('#view-work').on('click', function(){
	const images = $('#images').position().top;

	$('html, body').animate(
		{
			scrollTop: images
		},
		900
	);
});
